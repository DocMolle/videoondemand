angular.module('VOD', ['ngResource']);

function vodCtrl($scope, $resource) {
    $scope.vod = $resource('json/test.json',
        {callback:'JSON_CALLBACK'},
        {get:{method:'GET'}});

    $scope.vodResult = $scope.vod.get();

}